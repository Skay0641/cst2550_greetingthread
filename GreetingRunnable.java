import java.util.Date;
public class GreetingRunnable implements Runnable{

    private int reps;
    private int delay;
    private String greeting;
    
    public GreetingRunnable(String g, int r, int d){
	greeting = g;
	reps = r;
	delay = d;
    }
    
    public void run(){
	try {
	    
	    for (int i = 0; i < reps; ++i) {
		Date now = new Date();
		System.out.println(now + " " + greeting);
		Thread.sleep(delay);
	    }
	    
	} catch(InterruptedException ex){}
    }
}
